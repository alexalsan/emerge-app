// $(document).ready(init);
document.addEventListener("deviceready", init, false);

var nSteps = 4;
var stepActual = 1;

function init(){
    $('#flechaDer').click(flechaDerecha);
    $('#flechaIzq').click(flechaIzquierda);
	$('#circle_1').css({'background' : '#5da337'});
	$('.step_circle').click(clickCircle);
}

function flechaDerecha(){
	cambiarStep(stepActual+1);
}

function flechaIzquierda(){
	cambiarStep(stepActual-1);
}

//Esta es la función que se llama cuando se hace click en uno de los círculos
function clickCircle(){
	var idCircle = $(this).attr('id');
	idCircle = idCircle[idCircle.length-1]; //Nos quedamos con el último elemento, que es el número
	cambiarStep(parseInt(idCircle));
}

//Esta función actualiza el step en el que estamos
function cambiarStep(newStep){
	if (newStep > 0 && newStep < nSteps+1){

		var oldID = "#step_"+stepActual;
		var oldCircle = "#circle_"+stepActual;

		stepActual = newStep; 
		var nuevoID = "#step_"+stepActual;
		var nuevoCircle = "#circle_"+stepActual;

		$(oldID).fadeToggle( 400 );
		$(nuevoID).delay(400).fadeToggle( 400 );
		$(oldCircle).css({'background' : 'none'});
		$(nuevoCircle).css({'background' : '#5da337'});

	}
}
