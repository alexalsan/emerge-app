document.addEventListener("deviceready", init, false);

function init(){
    $('#botonContactar').click(enviarFormulario);
}

function enviarFormulario(){
	console.log("Intentando enviar mensaje...");
	//Primero comprobamos que los datos sean correctos
	var formularioCorrecto = checkFormulario();

	if (formularioCorrecto){
		console.log("Formulario correcto...");
		var nombre = $('#inputNombre').val();
		var apellido = $('#inputApellido').val();
		var email = $('#inputEmail').val();
		var mensaje = $('#inputMensaje').val();
		
		//cordova.plugins.email.isAvailable(
			// function (isAvailable) {
				// if(isAvailable){
					cordova.plugins.email.open({
						to:      'alexalmagrosantos@gmail.com',
						subject: '[EMERGE] Mensaje de: '+nombre+' '+apellido,
						body:    mensaje,
					});
				//}
			//}
		//);
	}
	//Si no hay errores, enviamos el correo
	// if (formularioCorrecto){
	// 	var asunto = "Mensaje de EmergeApp. Enviado por: "+nombre+" "+apellido+" ("+email+")";
	// 	var datos = 'asunto='+ asunto + '&mensaje=' + mensaje;

	// 	$.ajax({
	// 		type: "POST",
	// 		url: "../php/enviarCorreo.php",
	// 		data: datos,
	// 		success: function() {
	// 			alert("Mensaje enviado correctamente");
	// 		},
	// 		error:function (xhr, ajaxOptions, thrownError){
	// 			if(xhr.status==404) {
	// 				alert("Error: "+thrownError);
	// 			}
	// 		}
	// 	});
	// }
}

function checkFormulario(){
	var nombre = $('#inputNombre').val();
	var apellido = $('#inputApellido').val();
	var email = $('#inputEmail').val();
	var mensaje = $('#inputMensaje').val();
	var validacion_email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;

	//Comprobamos el nombre
	if ( (nombre == "") || (nombre == $('#inputNombre').attr('placeholder')) ){
		$('#inputNombre').focus();
		alert("Error. Debe introducir un nombre.");
		// $('#error_text_formulario').text("Error. Debe introducir un nombre.");
		return false;
	}

	//Comprobamos el apellido
	else if ( (apellido == "") || (apellido == $('#inputApellido').attr('placeholder')) ){
		$('#inputApellido').focus();
		alert("Error. Debe introducir un apellido.");
		// $('#error_text_formulario').text("Error. Debe introducir un apellido.");
		return false;
	}

	//Comprobamos el email
	else if ( (email == "") || (email == $('#inputEmail').attr('placeholder')) ){
		$('#inputEmail').focus();
		alert("Error. Debe introducir un email.");
		// $('#error_text_formulario').text("Error. Debe introducir un email.");
		return false;
	}
	else if(!validacion_email.test(email)){
		$('#inputEmail').focus();
		alert("Error. El formato del email no es válido.");
		// $('#error_text_formulario').text("Error. El formato del email no es válido.");
		return false;
	}

	//Comprobamos el mensaje
	else if ( (mensaje == "") || (mensaje == $('#inputMensaje').attr('placeholder')) ){
		$('#inputMensaje').focus();
		alert("Error. Debe introducir un mensaje.");
		// $('#error_text_formulario').text("Error. Debe introducir un mensaje.");
		return false;
	}

	return true;
}