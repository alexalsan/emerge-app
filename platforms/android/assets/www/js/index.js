/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

document.addEventListener("deviceready", init, false);

var menuAbierto = false;

function init(){
    $( "#menuLateral" ).load( "acercade.html #menuLateralOriginal" );
    $('.crossMark').click(clickCrossMark);

    // Hacemos que el botón físico de atrás siempre redirija al menú principal,
    // excepto cuando ya nos encontramos en este
    if ( document.URL.indexOf("index.html") == -1 ) { //Si NO estamos en la página principal
        document.addEventListener("backbutton", function(){
            window.location.href = "index.html";
        }, false);
    }
    //Si estamos en index.html y pulsamos el botón, simplemente salimos de la aplicación
    else{
        document.addEventListener("backbutton", function(){
            confirmation = confirm ("Confirme para abandonar la aplicación.");
            if (confirmation){
                navigator.app.exitApp();
            }   
        }, false);
    }
    
}

function clickCrossMark(){
    //id del elemento que se ha clickado
    var id = $(this).attr('id');

    switch(id){
        case 'crossNombre':
            $('#inputNombre').val("");
            $('#inputNombre').focus();
            break;
        case 'crossApellido':
            $('#inputApellido').val("");
            $('#inputApellido').focus();
            break;
        case 'crossEmail':
            $('#inputEmail').val("");
            $('#inputEmail').focus();
            break;
        case 'crossMensaje':
            $('#inputMensaje').val("");
            $('#inputMensaje').focus();
            break;
        case 'crossBusqMapa':
            $('#cuadroBusquedaMapa').val("");
            $('#cuadroBusquedaMapa').focus();
            break;
    }
}
