document.addEventListener("deviceready", init, false);

var imageGotten = false;
var exponente = -1;

var tempMaxHoy;
var tempMinHoy;

var cod_provincia;
var cod_estacion;

var especieEscogida;
var nombreEspecie, tempBaseEspecie, algoritmoEspecie, kEspecie, aEspecie, mCEspecie, dEspecie;

var fechaActual = new Date();
var fechaAnhoAtras = new Date();

//0-Abutilon theophrasti
//1-Cyperus rotundus
//2-Datura ferox
//3-Digitaria sanguinalis
//4-Echinochloa crus-galli
//5-Solanum nigrum
//6-Sonchus oleraceus
//7-Sorghum halepense
//8-Xanthium strumarium

function init(){
// 	$("#btnDownload").click(function() {
// 		alert("HO");
//  	    $("#myChart").get(0).toBlob(function(blob) {
//     		saveAs(blob, "chart_1.png");
// 		});
// 		alert("AL");
// });
	$('#btnDownload').click(downloadFile);

	//Calculamos la fecha de la búsqueda (desde hoy hasta hace 3 meses)
	
	fechaAnhoAtras.setMonth(fechaAnhoAtras.getMonth()-3);
	$( "#spanFechaBusqInicio" ).text(displayFecha(fechaAnhoAtras));
	$( "#spanFechaBusqFin" ).text(displayFecha(fechaActual));

    //Primero, mostramos el mapa con la ubicación actual
    var intervalo = setInterval(function(){
    	if (calcularPred){
    		$('#row1').addClass("hidden");
    		$('#row2').removeClass("hidden");  		

    		//estacionMasCercana();
    		getDatosEspecie();
    		cod_provincia=21;
    		cod_estacion=6;
    		//getDataFromWeb(displayFecha(fechaAnhoAtras),displayFecha(fechaActual));
    		//getDataFromWeb("09-06-2019","09-07-2019");
		    
		    //var predictionPercentageArray = getFinalResult(algoritmoEspecie, kEspecie,
		    	//aEspecie, mCEspecie, dEspecie);
    		//mostrarGrafico(predictionPercentageArray);
    		clearInterval(intervalo);
    	}
    },1000);
}


/* FUNCIONES PARA CALCULAR LA ESTACIÓN MÁS CERCANA
------------------------------------------------- */
function distanciaEuclidea (x1, y1, x2, y2){
    a = Math.pow(x2-x1, 2);
    b = Math.pow(y2-y1, 2);
    dist = Math.sqrt(a + b);

    return dist;
}

function estacionMasCercana(){
    var db = window.openDatabase("Database", "1.0", "Demo PhoneGap", 200000);
    db.transaction(queryDBEstacion, errorCB);
    //alert (distanciaEuclidea(2.1,3.2,4.3,5.5));
}

/* FUNCIONES DE LA BASE DE DATOS
------------------------------------------------- */
function getDatosEspecie(){
    var db = window.openDatabase("Database", "1.0", "Demo PhoneGap", 200000);
    db.transaction(queryDBEspecie, errorCB);
}

function queryDBEspecie(tx) {
    tx.executeSql('SELECT * FROM ESPECIES', [], querySuccessEspecie, errorCB);
}

function querySuccessEspecie(tx, results) {
    nombreEspecie = results.rows.item(especieEscogida).nombre;
    tempBaseEspecie = results.rows.item(especieEscogida).tb;
    algoritmoEspecie = results.rows.item(especieEscogida).funcion;
    kEspecie = results.rows.item(especieEscogida).k;
    aEspecie = results.rows.item(especieEscogida).a;
    mCEspecie = results.rows.item(especieEscogida).mc;
    dEspecie = results.rows.item(especieEscogida).d;

    getDataFromWeb(displayFecha(fechaAnhoAtras),displayFecha(fechaActual));
}

function errorCB(err) {
    alert("Error procesando SQL: "+err.code);
}

function queryDBEstacion(tx) {
    tx.executeSql('SELECT * FROM ESTACIONES', [], querySuccessEstacion, errorCB);
}

function querySuccessEstacion(tx, results) {
    var distanciaMenor = Infinity;
    var nuevaDistancia;
    var itemDistMenor;

    var latitud = $('#latHTML').text();
    var longitud = $('#lngHTML').text();

    for (var i = 0; i < results.rows.length; i++) {
        nuevaDistancia = distanciaEuclidea(latitud,longitud,results.rows.item(i).lat,results.rows.item(i).lng);        
        
        if (nuevaDistancia < distanciaMenor){
            distanciaMenor = nuevaDistancia;
            itemDistMenor = i;
        }
    }
    $( "#spanEstacion" ).text(results.rows.item(itemDistMenor).nombre);
    cod_provincia = results.rows.item(itemDistMenor).cod_prov;
    cod_estacion = results.rows.item(itemDistMenor).cod_est;
    // $('#ubicacion_text').text("La estación más cercana a su ubicación es: "+results.rows.item(itemDistMenor).nombre);
    anadirMarcador(results.rows.item(itemDistMenor).lat,results.rows.item(itemDistMenor).lng,"Estación: "+results.rows.item(itemDistMenor).nombre);
}

/* FUNCIONES PARA CALCULAR EL RESULTADO
------------------------------------------------- */
/* function getCurrentTemperature(){
	var tempTable;

	$.get(URLDatosActuales).then(function (html) {
        // Success response
        tempTable = $(html).find('#recordcontent > table > tbody > tr:nth-child(8) > td:nth-child(2)');
        var tempData = tempTable[0].innerHTML;
        $('#spanTemp').text(temp_data+"ºC");
        
        tempTable = $(html).find('#recordcontent > table > tbody > tr:nth-child(4) > td:nth-child(2)');
        tempMaxHoy = tempTable[0].innerHTML;
        tempTable = $(html).find('#recordcontent > table > tbody > tr:nth-child(6) > td:nth-child(2)');
        tempMinHoy = tempTable[0].innerHTML;
    }, function () {
        // Error response
        alert('Error. No se ha podido conseguir el dato');
    });
} */

function getDataFromWeb(fechaInicio,fechaFinal) {
	$.ajax({
		type: "POST",
		url: "https://www.juntadeandalucia.es/agriculturaypesca/ifapa/ria/servlet/FrontController?action=Static&url=datosHistoricos.jsp", // Example 
		data: { "c_provincia": cod_provincia, "c_estacion": cod_estacion, "FECHAINI": fechaInicio, "FECHAFIN": fechaFinal },
		contentType: "application/x-www-form-urlencoded",
		//success: function(response) {},
		success: generarOutput,
		error: function(e) {
		    alert('Error: ' + e.message);
		},
		async: false,
	});
}

function generarOutput(response){
	//Tomamos el número de filas
	var rows = $(response).find('#recordcontent > table > tbody > tr');
	var nRows = rows.length; //Última fila de los datos generados en la web. Son los datos más recientes.
   	var arrayDatosTemp = [];

   	console.log("Tomando datos...");

   	//Recorremos todas las filas tomando los datos
   	for (var i = nRows; i >= 2; i--) {
   		var queryStringMax = "#recordcontent > table > tbody > tr:nth-child("+i.toString()+") > td:nth-child(3)";
		var tempTableMax = $(response).find(queryStringMax);
	   	var tempDataMax = tempTableMax[0].innerHTML;

		var queryStringMin = "#recordcontent > table > tbody > tr:nth-child("+i.toString()+") > td:nth-child(5)";
	   	var tempTableMin = $(response).find(queryStringMin);
	   	var tempDataMin = tempTableMin[0].innerHTML;

	   	//Estamos tomando Strings, así que tenemos que convertirlos en Numbers
	   	var dayResult = [Number(tempDataMax),Number(tempDataMin)];
	    arrayDatosTemp.push(dayResult);
	}
	
	getFinalResult(arrayDatosTemp);
}

function getFinalResult (arrayDatosTemp){
	var cumulativeGDD = 0.0;
	var predictionPercentage = 0.0;
	var arrayPorcentaje = [];
	var arrayDate = [];

	console.log("Calculando resultado...");

	for (var i = 0; i < arrayDatosTemp.length; i=i+10) {
		if (predictionPercentage < 95.0){
			console.log("nIteraciones: "+i.toString()+", Porcentaje: "+predictionPercentage.toString());
			//En cada iteración tomamos 10 elementos del array
			var slicedTemperatureArray = arrayDatosTemp.slice(i,i+10); //Desde i hasta i+9
			var nuevaFecha = new Date();
			nuevaFecha.setDate(fechaAnhoAtras.getDate() + i); //Fecha que corresponde a la predicción
			console.log(displayFecha(nuevaFecha));

			cumulativeGDD = cumulativeGDD + getCumulativeGDD(slicedTemperatureArray);
			predictionPercentage = getPrediction(cumulativeGDD);
			arrayPorcentaje.push(predictionPercentage);
			arrayDate.push(nuevaFecha);
		}
	}
	
	mostrarGrafico(arrayPorcentaje, arrayDate);
}

function getCumulativeGDD(temperaturaArray){
	var i;
	var cumulativeGDD = 0;
	var gDD;

	for (i=0;i<temperaturaArray.length;i=i+1) 
	{
		tempMax = temperaturaArray[i][0];
		tempMin = temperaturaArray[i][1];
		gDD = (tempMax + tempMin)/2 - tempBaseEspecie;
		cumulativeGDD = cumulativeGDD + gDD;
	}
	return cumulativeGDD;
}

function getPrediction(cumulativeGDD){
	// algoritmoEspecie = 0, logistica | = 1, log. generalizada | = 2, Gompertz | = 3, Weibull
	var result;

	switch(algoritmoEspecie){
		case 0: //l-logistic algorithm
			result = kEspecie / (1 + Math.exp(-aEspecie * (cumulativeGDD-mCEspecie)));
			break;

		case 1: //e-generalised logistic algorithm
			result = kEspecie / Math.pow((1 + Math.exp(-aEspecie * (cumulativeGDD-mCEspecie))), dEspecie);
			break;

		case 2: //o-gompertz algorithm
			result = kEspecie * Math.exp(-1 * Math.exp(-aEspecie * (cumulativeGDD-mCEspecie)));
			break;

		case 3: //w-weibull algorithm
			result = kEspecie * (1 - Math.exp(-1 * Math.pow(aEspecie * cumulativeGDD, mCEspecie)));
			break;
	}

	return result;
}

/* FUNCIONES PARA MOSTRAR EL GRÁFICO
------------------------------------------------- */
function downloadFile(){
	console.log("Intentando descargar...");
	downloader.init({folder: "testApp", unzip: true});
	console.log("Inicializado correctamente...");
	downloader.get("contactar.html");
	console.log("Descargado...");
}

window.randomScalingFactor = function() {
		exponente = exponente + 1;
		//Redondeamos con dos decimales
		var resultado = Math.round(Math.pow(2.15,exponente)*100);
		return resultado/100;
};

function displayFecha(fecha){
	var mes = fecha.getMonth()+1;
	var display = (fecha.getDate()<10 ? '0' : '') + fecha.getDate() +
		'-' + (mes<10 ? '0' : '') + mes +
		'-' + fecha.getFullYear();
	return display;
}

function mostrarGrafico(predictionPercentageArray, dateArray){
	var ctx = $("#myChart");

	var timeFormat = 'MM/DD/YYYY';

	var resultsArray = [];
	var i;

	//Redondeamos a 2 decimales
	for (i=0; i<predictionPercentageArray.length;i++){
		resultsArray.push(Math.round(predictionPercentageArray[i] * 100) / 100);
	}

	var config = {
		type: 'line',
		data: {
			labels: dateArray, // Date Objects
			datasets: [{
				label: "Porcentaje de emergencia (%)",
				backgroundColor: "#5DA337",
				borderColor: "#5DA337",
				fill: false,
				data: resultsArray,
			}]
		},
		options: {
			responsive:true,
       		maintainAspectRatio: false,
			bezierCurve : false,
			animation: {
				onComplete: hideLoader,
  			},
			title: {
				text: 'Chart.js Time Scale'
			},
			scales: {
				xAxes: [{
					type: 'time',
					time: {
						parser: timeFormat,
						tooltipFormat: 'll'
					},
					scaleLabel: {
						display: true,
						labelString: 'Fecha'
					}
				}],
				yAxes: [{
					scaleLabel: {
						display: true,
						labelString: 'Porcentaje de emergencia (%)'
					}
				}]
			},
		}
	};

	var chart = new Chart(ctx, config);

	// $( "#spanEstacion" ).text( ""/*Colocar aquí la estación más cercana*/);
}

function hideLoader(){
	$('#myChart').removeClass("hidden");
	$('#loaderChart').addClass("hidden");
	$('#chart_text').addClass("hidden");
}

function getImage(){
	// Esta comprobación se hace para evitar que se actualice
	// la imagen cada vez que se hace click en el chart
	// (sólo se crea la imagen la primera vez)
	if (!imageGotten){
		var url=chart.toBase64Image();
		document.getElementById("url").src=url;
		imageGotten = true;
		// save64Img(url);
	}
}


