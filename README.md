# Emerge App

This project is born from the necessity of bringing new technologies to the agriculture sector in Spain. The final goal of the project was to create a mobile app which agriculture workers could use to predict the growth of harmful weeds using real-time data and mathematical algorithms. This makes it possible to significantly reduce the amount of herbicides used in the fields.

The target users are smalls farmers which do not have access to sofisticated and expensive measures to control harmful weeds. Therefore, one of the main objectives was to make the app as accessible and user-friendly as possible, since the final users are not necessarily familiar with new technologies.

This project was done individually, with the supervision and guidance of Jose Luis González (Dr., CSIC) and Pilar Martínez (Professor, Univerity of Córdoba).

## Technologies used
* JavaScript
* JQuery
* CSS
* HTML
* PhoneGap (framework)
* Interface design
* Interface prototype
* Mobile app
* Responsive design