$(document).ready(init);

var GDD=-1.0;
var result=-1;

//0-Abutilon theophrasti
//1-Cyperus rotundus
//2-Datura ferox
//3-Digitaria sanguinalis
//4-Echinochloa crus-galli
//5-Solanum nigrum
//6-Sonchus oleraceus
//7-Sorghum halepense
//8-Xanthium strumarium
var baseTempArray=[9.0,11.4,10.6,13.7,11.7,10.0,6.7,12.0,9.5];
var baseTemp = baseTempArray[especie]; //La temp. base de la especie que estamos tratando

function init(){
	getGDD();
}

function getGDD(tempMin, tempMax, baseTemp){
	return (tempMax + tempMin)/2 - baseTemp;
}

function getCumulativeGDD(temperaturaArray){
	var i;
	var cumulativeGDD = 0;

	//Pasamos i de 2 en 2 porque en cada pasada cogemos la temp. máxima y la temp. mínima
	for (i=0;i<temperaturaArray.length;i=i+2) 
	{
		cumulativeGDD = cumulativeGDD + getGDD(temperaturaArray[i], temperaturaArray[i+2]);
	}

	return cumulativeGDD;
}

function getFinalResult (){
	nMonths = -1; //0 indicaría datos de este mes (incluyendo hoy)
	var temperatureData;
	var cumulativeGDD = 0;
	var predictionPercentage;
	var finalResult = [];

	while (predictionPercentage < 0.95);
	{
		temperatureData = getTemperature(nMonths+1); 
		cumulativeGDD = cumulativeGDD + getCumulativeGDD(currentData);
		predictionPercentage = getPrediction(cumulativeGDD);
		finalResult.push(predictionPercentage);
	}

	return finalResult;
}

function getTemperature(estacion){
	switch(index){
		case 0: //Estación 1
			$.get('http://www.juntadeandalucia.es/agriculturaypesca/ifapa/ria/servlet/FrontController?action=Static&url=datosDiarios.jsp&c_provincia=14&c_estacion=7').then(function (html) {
	            // Success response
	            var temp_table = $(html).find('#recordcontent > table > tbody > tr:nth-child(8) > td:nth-child(2)');
	            var temp_data = temp_table[0].innerHTML;
	            $('#spanTemp').text(temp_data);

	            tempMax = temp_data;
	            tempMin = temp_data;
	        }, function () {
	            // Error response
	            $('#spanTemp').text('Error. No se ha podido conseguir el dato');
	            tempMax=-1.0;
	            tempMin=-1.0;
	        });
			break;
		case 1:

			break;

	}
}

function getPrediction(cumulativeGDD){
	//0-Abutilon theophrasti
	//1-Cyperus rotundus
	//2-Datura ferox
	//3-Digitaria sanguinalis
	//4-Echinochloa crus-galli
	//5-Solanum nigrum
	//6-Sonchus oleraceus
	//7-Sorghum halepense
	//8-Xanthium strumarium
	var algoritmo=['w','l','o','w','e','o','o','o','w'];
	
	var paramMC=[1.0,1045.3,302.8,0.8,525.4,793.3,1262.6,197.9,2.1];
	var paramA=[0.004,0.004,0.007,0.003,0.512,0.006,0.002,0.007,0.007];
	var paramK=[100.0,100.2,98.9,97.1,100.0,95.4,99.1,100.0,99.7];
	var paramD=[-1,-1,-1,-1,23.0,-1,-1,-1,-1]; //parámetro solo usado para el algoritmo gen.log.

	var m = paramMC[especie];
	var a = paramA[especie];
	var k = paramK[especie];
	var d = paramD[especie];

	switch(algoritmo[especie]){
		case 'l': //l-logistic algorithm
			result = k / (1 + Math.exp(-a * (cumulativeGDD-m)));
			break;

		case 'e': //e-generalised logistic algorithm
			result = k / Math.pow((1 + Math.exp(-a * (cumulativeGDD-m))), d);
			break;

		case 'o': //o-gompertz algorithm
			result = k * Math.exp(-1 * Math.exp(-a * (cumulativeGDD-m)));
			break;

		case 'w': //w-weibull algorithm
			result = k * (1 - Math.exp(-1 * Math.pow(a * cumulativeGDD, m)));
			break;
	}

	return result;
}