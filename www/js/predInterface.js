// $(document).ready(init);
document.addEventListener("deviceready", init, false);

const googleMapsScript = document.createElement('script');
googleMapsScript.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDTRKN61t3aoQaGGIKS4CEyqg4oK5aOEt8&callback=detectarUbicacion';
document.head.appendChild(googleMapsScript);

// var latitud;
// var longitud;
var latActual;
var lngActual;
var map;
var ubicacionEscogida = false;
var markers=[];
var calcularPred = false;

var especieEscogida;

function init(){
    //Primero, mostramos el mapa con la ubicación actual
    detectarUbicacion();
    //cuadroBusquedaMapa();

    $('#nextWindow').click(nextWindow);
}

/* PARA COMPROBAR QUE PODEMOS PASAR A LA SIGUIENTE PÁGINA 
------------------------------------------------- */
//Antes de ir a la siguiente página, comprobamos que todos los datos sean correctos
function nextWindow(){
    var especieOK = checkEspecie();

    if (especieOK){
        especieEscogida = $('#select_especie').val() - 1;
        $('#spanEspecieEscogida').text($( "#select_especie option:selected" ).text());
      //var ubicacionOK = checkUbicacion();

      //if (ubicacionOK){
        // Calculamos la predicción (se hace en predOperate.js)
        calcularPred = true;
      //}
    }
}

//Comprobamos que hayamos escogido una estación
function checkEspecie(){
    if ($('#select_especie').val() == "no_value"){
        alert("Debe elegir una especie.");
        $("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    }
    else{
        return true;
    }
}

function checkUbicacion(){
    if (!ubicacionEscogida){
        alert("Debe elegir una ubicación.");
        $("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    }
    else{
        return true;
    }
}

/* FUNCIONES DEL MAPA
------------------------------------------------- */
//Detectamos la ubicación del usuario y le decimos cuál es su estación más cercana
function detectarUbicacion(){
    // $('#ubicacionManual').prop({ 'disabled' : true});
    $('#mapa').css({ 'display' : 'block '});
    $('#ubicacion_text').text("Cargando mapa...");
    $('#loaderMap').removeClass("hidden");
    navigator.geolocation.getCurrentPosition(exitoMapa,errorMapa,{timeout: 15000});
}

function exitoMapa(position){
    latActual = position.coords.latitude;
    lngActual = position.coords.longitude;

    map = new google.maps.Map(document.getElementById('mapa'),{
        center: {lat: latActual, lng: lngActual},
        zoom: 15,
        disableDefaultUI: true,
        zoomControl: true,
        scaleControl: true,
        rotateControl: true,

        // panControl: true,
        // streetViewControl: false,
        // mapTypeControl: false,
        // fullscreenControl: false,
        // tap: tocarMapa,
        // click: tocarMapa,
    });

    map.addListener('click', function(e) {
        tocarMapa(e);
    });

    $('#ubicacion_text').text("");
    $('#loaderMap').addClass("hidden");

    //Una vez se ha cargado el mapa, podemos activar la funcionalidad de los botones
    $('#ubicacionAutomatica').click(ubicacionAutomatica);
}

function ubicacionAutomatica(){
    var confirmation = confirm ('¿Quieres detectar tu ubicación actual?');

    if (confirmation){
        //Primero, comprobamos que la ubicación elegida no esté fuera de España
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(latActual, lngActual);

        geocoder.geocode({'latLng': latlng}, function(results, status) {
            var fueraEspaña = true;

            if (status == google.maps.GeocoderStatus.OK) {
                for (var i=0; i<results[0].address_components.length; i++) {
                    if (results[0].address_components[i].short_name == "ES"){
                        fueraEspaña = false;
                        i = results[0].address_components.length;
                    }
                }

                //Si la ubicación marcada es válida
                if (!fueraEspaña){
                    borrarMarcadores();
                    anadirMarcador(latActual,lngActual,results[0].formatted_address);
                    ubicacionEscogida = true;
                }

                //Si la ubicación no es válida
                else{
                    alert("ERROR. Tu ubicación actual se encuentra fuera del territorio nacional.");
                }
            }

            else {
                alert("Error al cargar la ubicación marcada");
            }
        });
    }
}

function errorMapa(position){
    ubicacionEscogida = false;
    $('#loaderMap').addClass("hidden");
    $('#ubicacion_text').css({'color' : 'red'});
    $('#ubicacion_text').text("Error al cargar el mapa. Por favor comprueba que tu GPS está activado y vuelve a cargar la página.");
    $('#mapa').css({ 'display' : 'none '});
}

function anadirMarcador(latMarcador, lngMarcador, nombreMarcador){
    $('#latHTML').text(latMarcador);
    $('#lngHTML').text(lngMarcador);
    
    var infowindow = new google.maps.InfoWindow({
        content: nombreMarcador,
    });

    var marker = new google.maps.Marker({
        position: {lat: latMarcador,lng: lngMarcador},
        map: map,
        title: nombreMarcador,
    });

    marker.addListener('click', function() {
        infowindow.open(marker.get('map'), marker);
    });

    //Añadimos el nuevo marcador a nuestro array
    markers.push(marker);

    //Centramos el mapa respecto a ese marcador
    map.panTo({lat: latMarcador,lng: lngMarcador});
    // map.setCenter(latMarcador, lngMarcador);

    $('#ubicacion_text').css({'color' : 'black'});
    $('#ubicacion_text').text("UBICACIÓN ELEGIDA:\n"+nombreMarcador);
}

//Esta función borra el marcador que tuviéramos previamente
//y lo sustituimos por el que haya tocado el usuario
function tocarMapa(e){
    var confirmation = confirm ('¿Quieres usar esta ubicación?');

    if (confirmation){
        //Primero, comprobamos que la ubicación elegida no esté fuera de España
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());

        geocoder.geocode({'latLng': latlng}, function(results, status) {
            var fueraEspaña = true;

            if (status == google.maps.GeocoderStatus.OK) {
                for (var i=0; i<results[0].address_components.length; i++) {
                    if (results[0].address_components[i].short_name == "ES"){
                        fueraEspaña = false;
                        i = results[0].address_components.length;
                    }
                }

                //Si la ubicación marcada es válida
                if (!fueraEspaña){
                    borrarMarcadores();
                    anadirMarcador(e.latLng.lat(),e.latLng.lng(),results[0].formatted_address);
                    ubicacionEscogida = true;
                }

                //Si la ubicación no es válida
                else{
                    alert("ERROR. No puede seleccionar un lugar fuera del territorio nacional.");
                }
            }

            else {
                alert("Error al cargar la ubicación marcada");
            }
        });
    }
}

function borrarMarcadores(){
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
        markers.pop();
    }
}

function cuadroBusquedaMapa(){
    //Enlazamos la búsqueda de lugares y las predicciones con el cuadro de búsqueda
    var searchBox = new google.maps.places.SearchBox(document.getElementById('cuadroBusquedaMapa'));
    
    //Añadimos el siguiente eventListener, para que cuando se seleccione una opción,
    //aparezca un marcador en esa posición
    searchBox.addListener('places_changed', function() {
        var lugarEscogido = searchBox.getPlaces()[0];

        //Comprobamos que el lugar no esté fuera de España
        var fueraEspaña = true;

        for (var i=0; i<lugarEscogido.address_components.length; i++) {
            if (lugarEscogido.address_components[i].short_name == "ES"){
                fueraEspaña = false;
                i = lugarEscogido.address_components.length;
            }
        }

        if (fueraEspaña){
            alert("ERROR. No puede seleccionar un lugar fuera del territorio nacional.");
            $("#cuadroBusquedaMapa").val("");
        }
        else{
            //Añadimos el marcador
            borrarMarcadores();
            anadirMarcador(lugarEscogido.geometry.location.lat(), lugarEscogido.geometry.location.lng(), lugarEscogido.formatted_address);
            ubicacionEscogida = true;
        }
    });
}